var classClosedLoop_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoop_1_1ClosedLoop.html#ac4835c4506c54b79978bc59af3a58441", null ],
    [ "get_Kp", "classClosedLoop_1_1ClosedLoop.html#a38ef1cd742f356fcfad3e999b91e6242", null ],
    [ "set_Kp", "classClosedLoop_1_1ClosedLoop.html#a6e9e01c1ed577fbaca7f9e3de956f3e7", null ],
    [ "update", "classClosedLoop_1_1ClosedLoop.html#ab361595f86de2147b73019bfdf88abf2", null ],
    [ "deltaPWM", "classClosedLoop_1_1ClosedLoop.html#ab6cf7f5b8dd93e582f5c0ed3c2542e82", null ],
    [ "Kp", "classClosedLoop_1_1ClosedLoop.html#a600c2ee167190f2afdf9fc38c849cf01", null ],
    [ "PWM", "classClosedLoop_1_1ClosedLoop.html#aae6f845e7649c68d584f55cd6b7aeb3c", null ]
];