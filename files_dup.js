var files_dup =
[
    [ "ClosedLoop3.py", "ClosedLoop3_8py.html", [
      [ "ClosedLoop", "classClosedLoop3_1_1ClosedLoop.html", "classClosedLoop3_1_1ClosedLoop" ]
    ] ],
    [ "ClosedLoop4.py", "ClosedLoop4_8py.html", [
      [ "ClosedLoop", "classClosedLoop4_1_1ClosedLoop.html", "classClosedLoop4_1_1ClosedLoop" ]
    ] ],
    [ "CON_task2.py", "CON__task2_8py.html", [
      [ "CON_task", "classCON__task2_1_1CON__task.html", "classCON__task2_1_1CON__task" ]
    ] ],
    [ "CON_task3.py", "CON__task3_8py.html", [
      [ "CON_task", "classCON__task3_1_1CON__task.html", "classCON__task3_1_1CON__task" ]
    ] ],
    [ "CON_task4.py", "CON__task4_8py.html", [
      [ "CON_task", "classCON__task4_1_1CON__task.html", "classCON__task4_1_1CON__task" ]
    ] ],
    [ "EncoderDriver2.py", "EncoderDriver2_8py.html", [
      [ "EncoderDriver", "classEncoderDriver2_1_1EncoderDriver.html", "classEncoderDriver2_1_1EncoderDriver" ]
    ] ],
    [ "fibonacci.py", "fibonacci_8py.html", "fibonacci_8py" ],
    [ "FSM_Elevator.py", "FSM__Elevator_8py.html", "FSM__Elevator_8py" ],
    [ "LED_Pattern.py", "LED__Pattern_8py.html", "LED__Pattern_8py" ],
    [ "main2.py", "main2_8py.html", "main2_8py" ],
    [ "main3.py", "main3_8py.html", "main3_8py" ],
    [ "main4.py", "main4_8py.html", "main4_8py" ],
    [ "MorseCode.py", "MorseCode_8py.html", [
      [ "MorseCode", "classMorseCode_1_1MorseCode.html", "classMorseCode_1_1MorseCode" ]
    ] ],
    [ "MotorDriver3.py", "MotorDriver3_8py.html", [
      [ "MotorDriver", "classMotorDriver3_1_1MotorDriver.html", "classMotorDriver3_1_1MotorDriver" ]
    ] ],
    [ "MotorDriver4.py", "MotorDriver4_8py.html", [
      [ "MotorDriver", "classMotorDriver4_1_1MotorDriver.html", "classMotorDriver4_1_1MotorDriver" ]
    ] ],
    [ "shares2.py", "shares2_8py.html", "shares2_8py" ],
    [ "shares3.py", "shares3_8py.html", "shares3_8py" ],
    [ "shares4.py", "shares4_8py.html", "shares4_8py" ],
    [ "Simon_Says_class.py", "Simon__Says__class_8py.html", [
      [ "Simon_Says_class", "classSimon__Says__class_1_1Simon__Says__class.html", "classSimon__Says__class_1_1Simon__Says__class" ]
    ] ],
    [ "Simon_Says_main.py", "Simon__Says__main_8py.html", "Simon__Says__main_8py" ],
    [ "UI_dataGen1.py", "UI__dataGen1_8py.html", "UI__dataGen1_8py" ],
    [ "UI_front1.py", "UI__front1_8py.html", "UI__front1_8py" ],
    [ "UI_front2.py", "UI__front2_8py.html", "UI__front2_8py" ],
    [ "UI_front3.py", "UI__front3_8py.html", "UI__front3_8py" ],
    [ "UI_front4.py", "UI__front4_8py.html", "UI__front4_8py" ],
    [ "UI_task2.py", "UI__task2_8py.html", [
      [ "UI_task", "classUI__task2_1_1UI__task.html", "classUI__task2_1_1UI__task" ]
    ] ],
    [ "UI_task3.py", "UI__task3_8py.html", [
      [ "UI_task", "classUI__task3_1_1UI__task.html", "classUI__task3_1_1UI__task" ]
    ] ],
    [ "UI_task4.py", "UI__task4_8py.html", [
      [ "UI_task", "classUI__task4_1_1UI__task.html", "classUI__task4_1_1UI__task" ]
    ] ]
];