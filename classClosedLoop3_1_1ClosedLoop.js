var classClosedLoop3_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoop3_1_1ClosedLoop.html#a28b27043965ed87514ed63c6264b1724", null ],
    [ "get_Kp", "classClosedLoop3_1_1ClosedLoop.html#aed77fb602a508433075a7752387e3349", null ],
    [ "set_Kp", "classClosedLoop3_1_1ClosedLoop.html#a64a037f209b0e56e8b14b13fdf958b54", null ],
    [ "update", "classClosedLoop3_1_1ClosedLoop.html#af759056434817cd176ff974c22314abc", null ],
    [ "deltaPWM", "classClosedLoop3_1_1ClosedLoop.html#afc03ac2cd01ed2a55e718f5ce1d56277", null ],
    [ "Kp", "classClosedLoop3_1_1ClosedLoop.html#afe0f83598444db81a421b3982209b78f", null ],
    [ "PWM", "classClosedLoop3_1_1ClosedLoop.html#a2967623f4959f9c9f2d4931f406416de", null ]
];