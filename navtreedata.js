/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ahkar Kyaw_Portfolio", "index.html", [
    [ "Lab 0x00", "lab0.html", null ],
    [ "Lab 0x01", "lab1.html", null ],
    [ "Lab 0x02", "lab2.html", null ],
    [ "Lab 0x03", "lab3.html", null ],
    [ "Lab 0xFF", "labf.html", "labf" ],
    [ "Lab 0xFF: #1", "page1.html", [
      [ "Part 1: Extending Interface", "page1.html#sec", [
        [ "UI_front1.py", "page1.html#subsection1", null ],
        [ "UI_dataGen1.py", "page1.html#subsection2", null ]
      ] ]
    ] ],
    [ "Lab 0xFF: #2", "page2.html", [
      [ "Part 2: Incremental Encoders", "page2.html#sec2", [
        [ "UI_front2.py", "page2.html#subsection3", null ],
        [ "UI_task2.py", "page2.html#subsection4", null ],
        [ "CON_task2.py", "page2.html#subsection5", null ],
        [ "EncoderDriver2.py", "page2.html#subsection6", null ],
        [ "shares2.py", "page2.html#subsection7", null ],
        [ "main2.py", "page2.html#subsection8", null ]
      ] ]
    ] ],
    [ "Lab 0xFF: #3", "page3.html", [
      [ "Part 3: Motor Control", "page3.html#sec3", [
        [ "UI_front3.py", "page3.html#subsection9", null ],
        [ "UI_task3.py", "page3.html#subsection10", null ],
        [ "CON_task3.py", "page3.html#subsection11", null ],
        [ "EncoderDriver3.py", "page3.html#subsection12", null ],
        [ "MotorDriver3.py", "page3.html#subsection15", null ],
        [ "ClosedLoop3.py", "page3.html#subsection16", null ],
        [ "shares3.py", "page3.html#subsection17", null ],
        [ "main3.py", "page3.html#subsection18", null ]
      ] ]
    ] ],
    [ "Lab 0xFF: #4", "page4.html", [
      [ "Part 4: Reference Tracking", "page4.html#sec4", [
        [ "UI_task4.py", "page4.html#subsection19", null ],
        [ "UI_task4.py", "page4.html#subsection20", null ],
        [ "CON_task4.py", "page4.html#subsection21", null ],
        [ "EncoderDriver4.py", "page4.html#subsection22", null ],
        [ "MotorDriver4.py", "page4.html#subsection23", null ],
        [ "ClosedLoop4.py", "page4.html#subsection24", null ],
        [ "shares4.py", "page4.html#subsection25", null ],
        [ "main4.py", "page4.html#subsection26", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"CON__task2_8py.html",
"classSimon__Says__class_1_1Simon__Says__class.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';