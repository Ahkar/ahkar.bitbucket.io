var labf =
[
    [ "Introduction", "wk0.html", null ],
    [ "Extending Your Interface", "wk1.html", null ],
    [ "Incremental Encoders", "wk2.html", null ],
    [ "DC Motor Control", "wk3.html", null ],
    [ "Reference Tracking", "wk4.html", null ]
];