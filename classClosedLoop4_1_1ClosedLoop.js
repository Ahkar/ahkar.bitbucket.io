var classClosedLoop4_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoop4_1_1ClosedLoop.html#aef245b32fd25cf225191525908e5d1c7", null ],
    [ "get_Kp", "classClosedLoop4_1_1ClosedLoop.html#acf983250f417539e9d78635c4763bb5d", null ],
    [ "set_Kp", "classClosedLoop4_1_1ClosedLoop.html#afe726dfb97c6d6ea5078006d72778725", null ],
    [ "update", "classClosedLoop4_1_1ClosedLoop.html#ac0a0667aa6ccb4e56036753117407f0b", null ],
    [ "deltaPWM", "classClosedLoop4_1_1ClosedLoop.html#a66082072cda3cf35506d4e1fbac88486", null ],
    [ "Kp", "classClosedLoop4_1_1ClosedLoop.html#a53b6465c27d2a264bfaf993c52dfd176", null ],
    [ "PWM", "classClosedLoop4_1_1ClosedLoop.html#a7f7d69f8f439cd1c4cc7aee0d9507c28", null ]
];