var annotated_dup =
[
    [ "ClosedLoop3", null, [
      [ "ClosedLoop", "classClosedLoop3_1_1ClosedLoop.html", "classClosedLoop3_1_1ClosedLoop" ]
    ] ],
    [ "ClosedLoop4", null, [
      [ "ClosedLoop", "classClosedLoop4_1_1ClosedLoop.html", "classClosedLoop4_1_1ClosedLoop" ]
    ] ],
    [ "CON_task2", null, [
      [ "CON_task", "classCON__task2_1_1CON__task.html", "classCON__task2_1_1CON__task" ]
    ] ],
    [ "CON_task3", null, [
      [ "CON_task", "classCON__task3_1_1CON__task.html", "classCON__task3_1_1CON__task" ]
    ] ],
    [ "CON_task4", null, [
      [ "CON_task", "classCON__task4_1_1CON__task.html", "classCON__task4_1_1CON__task" ]
    ] ],
    [ "EncoderDriver2", null, [
      [ "EncoderDriver", "classEncoderDriver2_1_1EncoderDriver.html", "classEncoderDriver2_1_1EncoderDriver" ]
    ] ],
    [ "EncoderDriver3", null, [
      [ "EncoderDriver", "classEncoderDriver3_1_1EncoderDriver.html", "classEncoderDriver3_1_1EncoderDriver" ]
    ] ],
    [ "EncoderDriver4", null, [
      [ "EncoderDriver", "classEncoderDriver4_1_1EncoderDriver.html", "classEncoderDriver4_1_1EncoderDriver" ]
    ] ],
    [ "MorseCode", null, [
      [ "MorseCode", "classMorseCode_1_1MorseCode.html", "classMorseCode_1_1MorseCode" ]
    ] ],
    [ "MotorDriver3", null, [
      [ "MotorDriver", "classMotorDriver3_1_1MotorDriver.html", "classMotorDriver3_1_1MotorDriver" ]
    ] ],
    [ "MotorDriver4", null, [
      [ "MotorDriver", "classMotorDriver4_1_1MotorDriver.html", "classMotorDriver4_1_1MotorDriver" ]
    ] ],
    [ "Simon_Says_class", null, [
      [ "Simon_Says_class", "classSimon__Says__class_1_1Simon__Says__class.html", "classSimon__Says__class_1_1Simon__Says__class" ]
    ] ],
    [ "UI_task2", null, [
      [ "UI_task", "classUI__task2_1_1UI__task.html", "classUI__task2_1_1UI__task" ]
    ] ],
    [ "UI_task3", null, [
      [ "UI_task", "classUI__task3_1_1UI__task.html", "classUI__task3_1_1UI__task" ]
    ] ],
    [ "UI_task4", null, [
      [ "UI_task", "classUI__task4_1_1UI__task.html", "classUI__task4_1_1UI__task" ]
    ] ]
];